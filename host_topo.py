import subprocess
import shlex
import glob
import os
import sys
import pprint
import argparse

def run_cmd(cmd_list):
    ret = subprocess.check_output(cmd_list)
    return ret.split('\n')

def parse_kvm_device_opts(term):
    cfg_dict = {}
    for cfg in term.split(','):
        if '=' in cfg:
            k, v = cfg.split('=')
        else:
            k, v = cfg, True
        cfg_dict[k] = v
    return cfg_dict

def get_ovs_br(port):
    cmd = 'ovs-vsctl port-to-br %s' % port
    cmd_list = shlex.split(cmd)
    ret = [l for l in run_cmd(cmd_list) if l]
    return ret[0]

def get_lbr(port):
    intf_path = '/sys/class/net/%s/master' % port
    if os.path.exists(intf_path):
        return os.path.basename(os.readlink(intf_path))


def collect_br_data(db_path):
    intfs_path = '/sys/class/net/*/master'
    with open(db_path, 'w') as brdb:
        for intf_path in glob.glob(intfs_path):
            intf_name = intf_path.split('/')[4]
            br_name = os.path.basename(os.readlink(intf_path))
            if 'ovs-system' in br_name:
                br_name = 'ovs:%s' % get_ovs_br(intf_name)
            else:
                br_name = 'lbr:%s' % br_name
            brdb.write("%s@%s\n" % (intf_name, br_name))

def get_br_for_port(port, db_path):
    with open(db_path) as brdb:
        for line in brdb:
            nic, br = line.split('@')
            if nic == port:
                return br.split()[0]

def get_host_intf_name(pid, fd):
    with open('/proc/%s/fdinfo/%s' % (pid, fd)) as fdinfo:
        for line in fdinfo:
            if 'iff' in line:
                return line.split()[-1]

def get_nic_info(vm_nic, vm_pid, db_path, exclude_br_list):
    host_cfg, vm_cfg = vm_nic
    nic_host_info = parse_kvm_device_opts(host_cfg)
    nic_vm_info = parse_kvm_device_opts(vm_cfg)
    nic_host_fd = nic_host_info.get('fd')
    nic_vm_mac = nic_vm_info.get('mac')
    nic_vm_name = nic_vm_info.get('id')
    nic_host_name = get_host_intf_name(vm_pid, nic_host_fd)
    nic_br = get_br_for_port(nic_host_name, db_path)
    print "nic_br: %s exclude_list: %s" % (nic_br, exclude_br_list)
    if nic_br in exclude_br_list:
        print "=="
        return None
    return (nic_host_name, nic_vm_name, nic_vm_mac, nic_br)


def get_all_kvm_procs(vm_list, exclude_br_list):
    kvm_name_list = vm_list.get('name')
    kvm_uuid_list = vm_list.get('uuid')
    kvm_list = []
    db_path = '/tmp/db_path'
    cmd = 'virsh list --name'
    all_kvm_names = run_cmd(shlex.split(cmd))
    cmd = 'ps axww -o pid,cmd'
    all_procs = run_cmd(shlex.split(cmd))
    kvm_procs = [line for line in all_procs if 'kvm' in line]
    for kvm_name in all_kvm_names:
        if not kvm_name: continue
        cmd = 'virsh dominfo %s' % kvm_name
        uuid = [line for line in run_cmd(shlex.split(cmd))
                if 'UUID' in line][0].split()[-1]
        if kvm_name_list and kvm_name not in kvm_name_list:
            continue
        if kvm_uuid_list and uuid not in kvm_uuid_list:
            continue
        proc = [proc for proc in kvm_procs if uuid in proc][0]
        pid = proc.split()[0]
        cmd = proc.split()[1:]
        nics = [get_nic_info(ncfg, pid, db_path, exclude_br_list) for ncfg in get_nic_config(cmd)
                if get_nic_info(ncfg, pid, db_path, exclude_br_list)]

        kvm_cfg = {'name': kvm_name,
                   'uuid': uuid,
                   'pid': pid,
                   'nics': nics}
                   #'cmd': cmd,

        kvm_list.append(kvm_cfg)
    return kvm_list

def get_nic_config(cmdline):
    idx = 0
    nics = []
    for term in cmdline:
        if term.startswith('-netdev'):
            nics.append((cmdline[idx+1], cmdline[idx+3]))
        idx +=1
    return nics


def get_all_lxbr():
    return ['lbr:' + syspath.split('/')[4] for syspath in glob.glob('/sys/class/net/*/bridge')]

def get_all_ovsbr():
    cmd = 'ovs-vsctl list-br'
    cmd_list = shlex.split(cmd)
    ret = ['ovs:' + l for l in run_cmd(cmd_list) if l]
    return ret

def get_all_bridges(exclude_br_list):
    if not exclude_br_list:
        return get_all_lxbr() + get_all_ovsbr()
    else:
        return [br for br in get_all_lxbr() + get_all_ovsbr() 
                if br not in exclude_br_list]

def build_topology(vm_list, exclude_br_list):
    db_path = '/tmp/db_path'
    collect_br_data(db_path)
    topology = {'VMS': get_all_kvm_procs(vm_list, exclude_br_list),
                'BRIDGES': get_all_bridges(exclude_br_list)}

    return topology

def render_text(topology):
    pprint.pprint(topology)

def render_graphviz(topology):
    import pygraphviz as pgv

    brs = topology.get('BRIDGES')
    vms = topology.get('VMS')

    _graph = pgv.AGraph()

    for br in brs:
        _graph.add_node(br)
        node = _graph.get_node(br)
        node.attr['shape'] = 'box'
        node.attr['style'] = 'filled'
        node.attr['color'] = 'olivedrab3'

    for vm in vms:
        _graph.add_node(vm.get('name'))
        node = _graph.get_node(vm.get('name'))
        node.attr['shape'] = 'box'
        node.attr['color'] = 'lightblue2'
        node.attr['style'] = 'filled'
        for nic in vm.get('nics'):
            _graph.add_edge(vm.get('name'), nic[-1])
            

    _graph.layout(prog='dot')
    _graph.draw('topo.png')

def render_networkx(topology):
    br_group = 1
    vm_group = 2
    http_port = 8090

    import networkx as nx
    import json
    from networkx.readwrite import json_graph

    brs = topology.get('BRIDGES')
    vms = topology.get('VMS')
    _graph = nx.Graph()

    for br in brs:
        _graph.add_node(br, attr_dict={'group': br_group, 'type': 'bridge'})

    for vm in vms:
        _graph.add_node(vm.get('name'), attr_dict={'group': vm_group, 'type': 'vm'})
        for nic in vm.get('nics'):
            _graph.add_edge(vm.get('name'), nic[-1])

    for n in _graph:
        _graph.node[n]['name'] = n

    link_data = json_graph.node_link_data(_graph)
    json.dump(link_data, open('topo.json', 'w'))
    with open('topo.json') as jf:
        pprint.pprint(json.loads(jf.readline()))

    start_httpserver(http_port)


def start_httpserver(http_port):
    import SimpleHTTPServer
    import SocketServer


    Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
    httpd = SocketServer.TCPServer(("", http_port), Handler)
    print dir(httpd)

    try:
        print "Starting http server on port %s" % http_port
        print "Topology is available at http://0.0.0.0:%s/force.html" % http_port
        httpd.serve_forever()
    except KeyboardInterrupt:
        httpd.shutdown()
        print "Stopping http server"

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Visualize vm networking')
    parser.add_argument('--vm-list', dest='vm_list',
            help=('vm selection list can be specified as'
            'name=name1,name2,..nameN or '
            'uuid=uuid1,uuid2,..uuidN'))
    parser.add_argument('--bridge-exclude-list', dest='bridge_exclude_list',
            help=('specify bridges that should be excluded in graph'
                  ' can be specified as name=name1,name2..nameN'))

    parser.add_argument('--render', dest='render', default='text',
            help='Render as "text" json , "png" with graphviz, "d3" with networkx')
    parser.add_argument('--lsvms', action='store_true')
    parser.add_argument('--lsbrs', action='store_true')

    args = parser.parse_args()
    vm_list_conf = {}
    br_exclude_list = []
    if args.lsvms:
        pprint.pprint([kvm['name'] for kvm in
            get_all_kvm_procs(vm_list_conf, br_exclude_list)])
        sys.exit(0)

    if args.lsbrs:
        pprint.pprint(get_all_bridges(br_exclude_list))
        sys.exit(0)

    if args.vm_list:
        vm_list = args.vm_list
        if 'name' in vm_list:
            vm_list_conf['name'] = vm_list.split('=')[1].split(',')
        elif 'uuid' in vm_list:
            vm_list_conf['uuid'] = vm_list.split('=')[1].split(',')

    if args.bridge_exclude_list:
        br_exclude_list = args.bridge_exclude_list.split('=')[1].split(',')

    topo = build_topology(vm_list_conf, br_exclude_list)
    if args.render == 'text':
        render_text(topo)
    elif args.render == 'png':
        render_graphviz(topo)
    elif args.render == 'd3':
        render_networkx(topo)
